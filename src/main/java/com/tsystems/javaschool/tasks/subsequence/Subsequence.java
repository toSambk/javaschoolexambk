package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        return validation(x,y);
    }

    private static boolean objectsEqual(Object ob1, Object ob2) {
        if (ob1 == ob2) {
            return true;
        }
        if (ob1 == null || ob2 == null) {
            return false;
        }
        if (ob1.getClass() != ob2.getClass()) {
            return false;
        }
        if (ob1.getClass() == String.class) {
            String sOb1 = (String) ob1;
            String sOb2 = (String) ob2;
            return sOb1.equals(sOb2);
        } else if (ob1.getClass() == Integer.class) {
            Integer iOb1 = (Integer) ob1;
            Integer iOb2 = (Integer) ob2;
            return iOb1.equals(iOb2);
        }
        return false;
    }

    private static boolean validation(List x, List y) {
        if (x == null || y == null) {
            throw new IllegalArgumentException();
        }
        if (x.size() == 0) {
            return true;
        }
        int counterY = 0;
        boolean result = false;
        for (int i = 0; i < x.size(); i++) {
            for (int j = counterY; j < y.size(); j++, counterY++) {
                System.out.println(y.get(j));
                if (objectsEqual(x.get(i), y.get(j))) {
                    if (i == x.size() - 1) {
                        result = true;
                        break;
                    }
                    counterY++;
                    break;
                }
            }
        }
        return result;
    }
}
