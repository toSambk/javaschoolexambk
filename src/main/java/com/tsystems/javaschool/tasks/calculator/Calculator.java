package com.tsystems.javaschool.tasks.calculator;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        return checking(statement)? null : rounding(returnResult(statement));
    }

    private static String returnResult(String statement) {
        int openBracket = 0, closeBracket = statement.length(), brackets = 0;
        boolean bracketOpened = false;
        if (statement == null) {
            return null;
        }
        for (int i = 0; i < statement.length(); i++) {
            if (statement.charAt(i) == '(' && i >= openBracket) {
                openBracket = i;
                bracketOpened = true;
            }
            if(statement.charAt(i) == '(' || statement.charAt(i) == ')') {
                brackets++;
            }
        }
        if (brackets%2!=0) {
            return null;
        }
        for (int i = 0; i < statement.length(); i++) {
            if ( i > openBracket && statement.charAt(i) == ')' && bracketOpened) {
                closeBracket = i;
                break;
            }
        }
        if (openBracket == 0 && closeBracket == statement.length()) {
            return bracketManage("(" + statement + ")");
        } else {
            return returnResult(statement.substring(0, openBracket)
                    + bracketManage(statement.substring(openBracket, closeBracket + 1))
                    + statement.substring(closeBracket + 1));
        }
    }

    private static String bracketManage(String statement) {
        return bracketSumSub(bracketMulDiv(statement.substring(1, statement.length()-1)));
    }

    private static String bracketMulDiv (String statement) {
        if (statement == null) {
            return null;
        }
        int operatorInd = 0, prevIndex = 0, nextIndex = statement.length();
        char operator = 'a';
        boolean catchFlag = false;
        for(int i = 0; i < statement.length(); i++) {
            if ((statement.charAt(i) == '+' || statement.charAt(i) == '-') && !catchFlag) {
                prevIndex = i;
            }
            if ((statement.charAt(i) == '*' || statement.charAt(i) == '/') && !catchFlag) {
                operatorInd = i;
                operator = statement.charAt(i);
                catchFlag = true;
                continue;
            }
            if ((statement.charAt(i) == '+' || statement.charAt(i) == '-'
                    || statement.charAt(i) == '*' || statement.charAt(i) == '/') && catchFlag ) {
                if (Math.abs(i-operatorInd) > 1) {
                    nextIndex = i;
                    break;
                }
            }
        }
        if (catchFlag) {
           return bracketMulDiv(multiplyDivide(operator,prevIndex,operatorInd,nextIndex,statement));
        } else {
            return statement;
        }
    }

    private static String bracketSumSub(String statement) {
        if (statement == null) {
            return null;
        }
        int nextIndex = statement.length(), operatorInd = 0;
        char operator = 'a';
        boolean catchFlag = false;
        for(int i = 0; i < statement.length(); i++) {

            if (i == 0 && (statement.charAt(i) == '+' || statement.charAt(i) == '-')) {
                continue;
            }
            if ((statement.charAt(i) == '+' || statement.charAt(i) == '-') && !catchFlag) {
                operator = statement.charAt(i);
                operatorInd = i;
                catchFlag = true;
                continue;
            }
            if ((statement.charAt(i) == '+' || statement.charAt(i) == '-') && catchFlag) {
                nextIndex = i;
                break;
            }
        }
        if (catchFlag) {
            return bracketSumSub(sumSub(operator, operatorInd, nextIndex, statement));
        } else {
            return statement;
        }
    }

    private static String multiplyDivide (char operator, int prevIndex, int operatorInd, int nextIndex, String statement) {
        double result = 0;
        if (prevIndex!=0) {
            prevIndex+=1;
        }
        if (operator == '*') {
            result =  Double.parseDouble(statement.substring(prevIndex, operatorInd))
                    * Double.parseDouble(statement.substring(operatorInd + 1, nextIndex));

            return statement.substring(0, prevIndex) + result + statement.substring(nextIndex);
        } else {
                boolean divZero = false;
                if (Double.parseDouble(statement.substring(operatorInd + 1, nextIndex)) != 0) {
                    result = Double.parseDouble(statement.substring(prevIndex, operatorInd))
                            / Double.parseDouble(statement.substring(operatorInd + 1, nextIndex));
                } else {
                    divZero = true;
                }
            return divZero? null : statement.substring(0, prevIndex) + result + statement.substring(nextIndex);
        }
    }

    private static String sumSub (char operator, int operatorInd, int nextInd, String statement) {
        double result = 0;
        if (operator == '+') {
            result = Double.parseDouble(statement.substring(0, operatorInd))
                    + Double.parseDouble(statement.substring(operatorInd + 1, nextInd));
            return result + statement.substring(nextInd);
        } else {
            result = Double.parseDouble(statement.substring(0, operatorInd))
                    - Double.parseDouble(statement.substring(operatorInd + 1, nextInd));
            return result + statement.substring(nextInd);
        }
    }

    private static String rounding(String statement) {
        if (statement == null) {
            return null;
        }
        DecimalFormat df = new DecimalFormat("#.####");
        df.setRoundingMode(RoundingMode.HALF_UP);
        return "" + df.format(Double.parseDouble(statement)).replace(',', '.');
    }

    private static boolean checking(String statement) {
        boolean error = false, rotation = false;
        if (statement == null || statement.length() == 0) {
            error = true;
            return error;
        }
        statement = statement.trim();
        Character[] arr = new Character[]{'1','2','3','4','5','6','7','8','9','0','(',')','/','*','+','-','.'};
        List<Character> list = Arrays.asList(arr);
        char prevChar = 'a';
        int prevIndex = 0;
        for (int i = 0; i < statement.length(); i++) {
            if (!list.contains(statement.charAt(i))) {
                error = true;
                break;
            }
            boolean comp = statement.charAt(i) == '.' || statement.charAt(i) == '-' || statement.charAt(i) == '+'
                    || statement.charAt(i) == '*' || statement.charAt(i) == '/';
            if (comp && !rotation) {
                prevChar = statement.charAt(i);
                prevIndex = i;
                rotation = true;
                continue;
            }

            if (comp && rotation) {
                if (prevChar == statement.charAt(i) && Math.abs(i - prevIndex) <=1) {
                    error = true;
                    break;
                } else {
                    prevChar = statement.charAt(i);
                    prevIndex = i;
                }
            }
        }
        return error;
    }

}
