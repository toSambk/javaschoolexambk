package com.tsystems.javaschool.tasks.pyramid;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) throws CannotBuildPyramidException {
       List<Integer> rowsColumns =  returnRows(inputNumbers);
       return process(inputNumbers, rowsColumns.get(0), rowsColumns.get(1));
    }

    private static List<Integer> returnRows(List<Integer> inputNumbers) throws CannotBuildPyramidException {
        int remainder = inputNumbers.size(), rows = 0, columns = 0;
        boolean finish = false, error = false;
        for(int i = 1; !finish ; i++) {
            remainder = remainder - i;
            rows++;
            if (remainder == 0) {
                finish = true;
                columns = 2*i - 1;
            }
            if (remainder < 0) {
                error = true;
                finish = true;
            }
        }
        for (Integer i : inputNumbers) {
            if(i == null) {
                error = true;
                break;
            }
        }
        if (error) {
            throw new CannotBuildPyramidException();
        }
        return Arrays.asList(rows, columns);
    }

    private static int[][] process(List<Integer> list, int rows, int columns) {
       int [][] result = new int[rows][columns];
       Collections.sort(list);
       int startPosition = columns/2, counter = 0, numbersInLine = 1, numbers = 0;
       for (int i = 0; i < result.length; i++) {
           for(int j = 0; j < result[i].length; j++) {
                    if (j == startPosition) {
                        for(int m = 0; numbers < numbersInLine; m=m+2) {
                        result[i][j + m] = list.get(counter);
                        counter++;
                        numbers++;
                    }
                        break;
                }
           }
           startPosition--;
           numbersInLine++;
           numbers = 0;
       }
       return result;
    }


}
